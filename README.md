# Gitlab backup with restic

## Overview

The main goal of this project is to set up a backup of Gitlab files on Google Drive using restiс

But, in order to test the functionality, we create a new instance of the Gitlab server using Terraform and Ansible.

There are 2 options for creating a Google Cloud service account: manually with the GCP web interface or using ansible code

In any case, you need to have a Google Cloud account with the appropriate permissions. In addition, in case you are going to create a service account using ansible code, on the PC where the terraform and ansible code will be running, `gcloud cli` must be installed with the selected active account (please use the command: `gcloud auth login`). If you create a service account and its key manually, you must place the account JSON file here:`<yor_local_home_dir>/.gcp/<your_sa_credential_file>.json`


---
## Infrastructure Provisioning using Terraform
### If Gitlab server is already installed, just skip this step

To create a virtual server for deploying the Gitlab server, Terraform is used with the HETZNER provider. You can find more information on how to install Terraform [here](https://www.terraform.io/downloads.html).


When creating the Terraform manifests, local modules are used. These modules are stored in a local repository.


## Ansible playbooks
You can find more information on how to install Ansible [here](https://docs.ansible.com/ansible/latest/installation_guide/index.html)

#### Overview

This Ansible playbook is used to deploy a Gitlab cluster and set up its backup:

- `ansible/main.yaml`

The `main.yaml` playbook consists of three roles.

##### Ansible Roles:
1. ansible-role-gitlab        | for gitlab deployment (if you don't need to install Gitlab server, just comment out this role)  
2. gcp-sa                     | to create a Google service account and its key (if you already have a service account and its key, just comment out this role)
3. restic-backup              | for installing and configuring restic and rclone


#### Steps to Deploy

1. Fill in all the variables in file ansible/group_vars/all/vault.yaml
    - localhost_user_profile: "/home/user"                                       ## user profile on localhost 
    - remote_user_profile: "/root"                                               ## user profile on gitlab server
    - gcp_project: "Your_GCP_Project_Id"
    - gcp_service_account_name: "gcp-backup-sa"
    - gcp_service_account_email: "{{ gcp_service_account_name }}@{{ gcp_project }}.iam.gserviceaccount.com"   # service account email for Id   
    - local_gcp_service_account_key: "{{ localhost_user_profile }}/.gcp/gcp_backup_sa.json"                   # place for service account credentials on localhost
    - restic_password: "Your_RESTIC_Password"                                    ##   Change it in production
    - restic_repo: "rclone:google_drive:gitlab_backup"
    - backup_script_path: "/usr/local/bin/gitlab_backup.sh"
    - rclone_config_path: "{{ remote_user_profile }}/.config/rclone"             ##   don't touch it
    - folder_id: "Your_backup_folder_id_on_google_drive"                         ##   Your google drive's folder id
2. Fill in all the variables in file terraform/terraform.tfvars 
   - gitlab_server_name = "gitlab-server-name"
   - cert_manager_email   = "email"
   - primary_domain       = "your_domain"
   - ansible_ssh_keys = [
       {
         key = "your_ssh_public_key"
       }
     ]
   - hcloud_token = "your_hcloud_token"
   

3. Create a folder on your Google drive and give editor rights to the service account we are going to create. His name is indicated in the gcp_service_account_name variable, you need to specify his email: i.e. gcp-backup-sa@my-gcp-project.iam.gserviceaccount.com (variable {{ gcp_service_account_email }})
4. Fill in the variable "folder_id" in file ansible/group_vars/all/vault.yaml with your just created folder_id 
5. cd project_root/terraform
6. terraform init && terraform plan && terraform apply
7. cd project_root/ansible
8. ansible-playbook main.yaml -vv