module "gitlab_server" {
  source           = "./modules/server"
  server_name      = var.gitlab_server_name
  server_type      = var.gitlab_server_type
  ansible_ssh_keys = var.ansible_ssh_keys
  providers = {
    hcloud = hcloud
  }
}
