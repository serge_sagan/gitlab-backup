output "host" {
  value = resource.hcloud_server.server.ipv4_address
}

output "terraform_ssh_key" {
  value     = resource.tls_private_key.terraform_ssh_key.private_key_openssh
  sensitive = true
}

