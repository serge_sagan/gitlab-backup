resource "tls_private_key" "terraform_ssh_key" {
  algorithm = "ED25519"
}

resource "local_sensitive_file" "terraform_ssh_key" {
  content  = resource.tls_private_key.terraform_ssh_key.private_key_openssh
  filename = "${path.module}/${var.server_name}-terraform.pem"
}

resource "hcloud_ssh_key" "default" {
  name       = "Terraform Managed ${var.server_name}"
  public_key = resource.tls_private_key.terraform_ssh_key.public_key_openssh
}

resource "hcloud_server" "server" {
  name        = var.server_name
  image       = var.server_image
  server_type = var.server_type
  datacenter  = var.datacenter
  ssh_keys    = [resource.hcloud_ssh_key.default.id]
  public_net {
    ipv4_enabled = true
    ipv6_enabled = false
  }
}

resource "time_sleep" "wait_30_seconds" {
  depends_on = [resource.hcloud_server.server]

  create_duration = "30s"
}

resource "ansible_playbook" "gitlab" {
  replayable = true
  name       = resource.hcloud_server.server.name
  playbook   = "${path.module}/ansible/playbook.yml"
  #groups     = ["ssh", "firewall", "gitlab"]
  groups     = ["ssh"]
  extra_vars = {
    ansible_host                 = resource.hcloud_server.server.ipv4_address,
    ansible_user                 = "root",
    ansible_ssh_private_key_file = "${path.module}/${var.server_name}-terraform.pem",
    ssh_keys                     = jsonencode(var.ansible_ssh_keys),
  }
  ignore_playbook_failure = false
  limit                   = [var.server_name]
  depends_on              = [resource.time_sleep.wait_30_seconds]
}


resource "null_resource" "cleanup" {
  depends_on = [
    resource.ansible_playbook.gitlab
  ]
  triggers = {
    null_id = resource.local_sensitive_file.terraform_ssh_key.id
  }
  provisioner "local-exec" {
    command = "rm -f ${path.module}/${var.server_name}-terraform.pem"
  }
}