variable "hcloud_token" {
  sensitive = true
}

variable "ansible_ssh_keys" {
  description = "List of SSH Public Keys passed to Ansible Provider"
  type        = list(map(string))
  default     = []
}

variable "cert_manager_email" {
  description = "Email for certificate notifications"
  type        = string
  default     = "admin@example.com"
}

variable "gitlab_server_name" {
  description = "Gitlab Server Name"
  type        = string
  default     = "gitlab-test"
}

variable "gitlab_server_type" {
  description = "Gitlab Server Type"
  type        = string
  default     = "cax21"
}

variable "primary_domain" {
  description = "Primary Domain"
  type        = string
  default     = "example.com"
}

variable "gitlab_url" {
  description = "Gitlab URL"
  type        = string
  default     = "https://gitlab.com"
}

variable "gitlab_base_url" {
  description = "Gitlab Base URL"
  type        = string
  default     = "https://gitlab.com/api/v4/"
}

variable "gitlab_access_token" {
  description = "Gitlab Access Token"
  type        = string
  default     = ""
}

variable "gitlab_infra_project" {
  description = "Gitlab project path to get Secret Variables from"
  type        = string
  default     = "path-to/infra"
}
