terraform {
  required_version = ">= 1.0"

  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.44.1"
    }
    # gitlab = {
    #   source  = "gitlabhq/gitlab"
    #   version = "16.7.0"
    # }
  }
}
