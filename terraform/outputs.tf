output "gitlab_host" {
  value = module.gitlab_server.host
}

output "terraform_ssh_key" {
  value     = module.gitlab_server.terraform_ssh_key
  sensitive = true
}
